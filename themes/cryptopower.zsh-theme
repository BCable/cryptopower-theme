#white vim:ft=zsh ts=2 sw=2 sts=2
#
# cryptopower Theme
# A Powerline-inspired theme for ZSH, based on agnoster's Theme
#
# # README (of agnoster's theme)
#
# In order for this theme to render correctly, you will need a
# [Powerline-patched font](https://github.com/Lokaltog/powerline-fonts).
#
# In addition, I recommend the
# [Solarized theme](https://github.com/altercation/solarized/) and, if you're
# using it on Mac OS X, [iTerm 2](http://www.iterm2.com/) over Terminal.app -
# it has significantly better color fidelity.
#
# # README (of cryptopower theme)
#
# This theme also requires the "bc" and "openssl" utilities to be installed,
# which are standardized UNIX tools available on most UNIX distributions.
#
# For better BSD compatibility, setting LC_ALL in your ~/.zshrc to something
# like "en_US.UTF-8" or another unicode friendly format will get powerline to
# work better remotely.  This prompt will likely break without this setting.
#
# Two additional options are available to put in your ~/.zshrc called
# ZSH_CRYPTOPOWER_UID and ZSH_CRYPTOPOWER_USER.  These options provides
# cryptopower with the knowledge of the default UID/username of your accounts on
# your local and remote servers so that cryptopower always uses the default
# white for your default username while providing a different color for
# alternate users.  See the "Goals" section and screenshot for more information.
#
# Examples of usage of these options, each can be used individually or at the
# same time to provide the effect shown in the screenshot:
#
#   ZSH_CRYPTOPOWER_UID=43121
#   ZSH_CRYPTOPOWER_USERS=(brad bscable)
#
# # Goals (of agnoster's theme)
#
# The aim of this theme is to only show you *relevant* information. Like most
# prompts, it will only show git information when in a git working directory.
# However, it goes a step further: everything from the current user and
# hostname to whether the last call exited with an error to whether background
# jobs are running in this shell will all be displayed automatically when
# appropriate.
#
# # Goals (of cryptopower theme)
#
# Building on the goals of agnoster's theme, cryptopower is designed for people
# who log into a lot of remote servers and do system administration.  This theme
# will always display the host you are on and provide a specific color related
# to the hostname you are connected to.  This color will always be the same
# every time you are logged into that host (remote or local), but each host will
# vary between five colors.  You do not have to set which host gets what color
# it is assigned by calculating a number based off of cryptographically hashing
# the hostname, which means you can log into a new server and still get a
# unique-ish color for it that will always be the same.  This helps distinguish
# which server you are on at a glance, without having to build a list and
# synchronize anything.
#
# Your default username, assuming your UID or usernames are defined by the
# properties explained in the README, will always be white.  "root" will always
# show up as red to inform you that your current shell has superuser privileges.
# Any other user, for instance if you log in as another user through various
# superuser commands, will show up as a seeded random color just like the
# hostname, excluding red and white to differentiate the users.
#
# This theme also provides an RPROMPT that tells the current time for telling
# when long commands finish without taking up extra space at your prompt.  This
# prompt shows up in the same color as the host you are currently logged into.

### Segment drawing
# A few utility functions to make it easy and re-usable to draw segmented prompts

CURRENT_BG='NONE'
SEGMENT_SEPARATOR='\uE0B0'
SEGMENT_HOLLOW_SEPARATOR='\uE0B1'
RSEGMENT_SEPARATOR='\uE0B2'
RSEGMENT_HOLLOW_SEPARATOR='\uE0B3'

# Begin a segment
# Takes two arguments, background and foreground. Both can be omitted,
# rendering default background/foreground.
prompt_segment() {
  local lbg lfg fg_current_bg
  [[ -n $1 ]] && lbg="%K{$1}" || lbg="%k"
  [[ -n $2 ]] && lfg="%F{$2}" || lfg="%f"
  if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
    echo -n " %{$lbg%F{$CURRENT_BG}%}$SEGMENT_SEPARATOR%{$lfg%} "
  else
    echo -n "%{$lbg%}%{$lfg%} "
  fi
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
}

rprompt_segment() {
  local lbg lfg fg_current_bg
  [[ -n $1 ]] && lbg="%K{$1}" || lbg="%k"
  [[ -n $2 ]] && lfg="%F{$2}" || lfg="%f"
  echo -n " %{$lfg%F{$1}%}$RSEGMENT_SEPARATOR$lfg$lbg "
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
}

# End the prompt, closing any open segments
prompt_end() {
  if [[ -n $CURRENT_BG ]]; then
    echo -n " %{%k%F{$CURRENT_BG}%}$SEGMENT_SEPARATOR"
  else
    echo -n "%{%k%}"
  fi
  echo -n "%{%f%}"
  CURRENT_BG=''
}

# gets a seeded color based on hashing the input string
prompt_color() {
  local hash_hex hash_dec
  if [[ -z $1 ]]; then
    echo -n white
  else
    hash_hex=`echo -n "$1" | openssl dgst -md5 | cut -d ' ' -f2 | tr "abcdef" "ABCDEF"`
    # convert to decimal, modulus by 5 valid colors, start at 2
    hash_dec=`echo "obase=10;ibase=16;$hash_hex%5+2" | bc`
    echo -n "$hash_dec"
  fi
}

# gets the appropriate foreground color
prompt_fgcolor() {
  local bgcolors fgcolors index
  bgcolors=(black red green yellow blue magenta cyan white)
  fgcolors=(white white black black white white black black)
  index=${bgcolors[(i)$1]}
  if [[ $index -gt ${#bgcolors} ]]; then
    let index=$1+1
  fi
  echo -n "$fgcolors[$index]"
}

# gets whether or not the user is a default user
is_default_user() {
  if [[ "$UID" = "$ZSH_CRYPTOPOWER_UID" ]]; then
    echo 1
  elif [[ ${#ZSH_CRYPTOPOWER_USERS} -gt 0 ]]; then
    index=${ZSH_CRYPTOPOWER_USERS[(i)$USER]}
    if [[ $index -le ${#ZSH_CRYPTOPOWER_USERS} ]]; then
      echo 1
    fi
  fi
}

### Prompt components
# Each component will draw itself, and hide itself if no information needs to be shown

# Context: user@hostname (who am I and where am I)
prompt_context() {
  local user user_fg_color host_bg_color host_fg_color user_bg_color

  user="$USER"
  if [[ "$UID" = "0" ]]; then
    user_bg_color="red"
    user="root"
  elif [[ ! -z $(is_default_user) ]]; then
    user_bg_color="white"
  else
    user_bg_color=`prompt_color "$user"`
  fi
  user_fg_color=`prompt_fgcolor "$user_bg_color"`
  prompt_segment "$user_bg_color" "$user_fg_color" "$user"

  host_bg_color=`prompt_color "$HOST"`
  host_fg_color=`prompt_fgcolor "$host_bg_color"`
  prompt_segment "$host_bg_color" "$host_fg_color" "%m"
}

# Git: branch/detached head, dirty status
prompt_git() {
  local ref dirty mode repo_path
  repo_path=$(git rev-parse --git-dir 2>/dev/null)

  if $(git rev-parse --is-inside-work-tree >/dev/null 2>&1); then
    dirty=$(parse_git_dirty)
    ref=$(git symbolic-ref HEAD 2> /dev/null) || ref="➦ $(git show-ref --head -s --abbrev |head -n1 2> /dev/null)"
    if [[ -n $dirty ]]; then
      prompt_segment yellow black
    else
      prompt_segment green black
    fi

    if [[ -e "${repo_path}/BISECT_LOG" ]]; then
      mode=" <B>"
    elif [[ -e "${repo_path}/MERGE_HEAD" ]]; then
      mode=" >M<"
    elif [[ -e "${repo_path}/rebase" || -e "${repo_path}/rebase-apply" || -e "${repo_path}/rebase-merge" || -e "${repo_path}/../.dotest" ]]; then
      mode=" >R>"
    fi

    setopt promptsubst
    autoload -Uz vcs_info

    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' stagedstr '✚'
    zstyle ':vcs_info:git:*' unstagedstr '●'
    zstyle ':vcs_info:*' formats ' %u%c'
    zstyle ':vcs_info:*' actionformats ' %u%c'
    vcs_info
    echo -n "${ref/refs\/heads\// }${vcs_info_msg_0_%% }${mode}"
  fi
}

prompt_hg() {
  local rev status
  if $(hg id >/dev/null 2>&1); then
    if $(hg prompt >/dev/null 2>&1); then
      if [[ $(hg prompt "{status|unknown}") = "?" ]]; then
        # if files are not added
        prompt_segment red white
        st='±'
      elif [[ -n $(hg prompt "{status|modified}") ]]; then
        # if any modification
        prompt_segment yellow black
        st='±'
      else
        # if working copy is clean
        prompt_segment green black
      fi
      echo -n $(hg prompt "☿ {rev}@{branch}") $st
    else
      st=""
      rev=$(hg id -n 2>/dev/null | sed 's/[^-0-9]//g')
      branch=$(hg id -b 2>/dev/null)
      if `hg st | grep -Eq "^\?"`; then
        prompt_segment red white
        st='±'
      elif `hg st | grep -Eq "^(M|A)"`; then
        prompt_segment yellow black
        st='±'
      else
        prompt_segment green black
      fi
      echo -n "☿ $rev@$branch" $st
    fi
  fi
}

# Dir: current working directory
prompt_dir() {
  prompt_segment white black '%~'
}

# Virtualenv: current working virtualenv
prompt_virtualenv() {
  local virtualenv_path="$VIRTUAL_ENV"
  if [[ -n $virtualenv_path && -n $VIRTUAL_ENV_DISABLE_PROMPT ]]; then
    prompt_segment blue black "(`basename $virtualenv_path`)"
  fi
}

# Status:
# - was there an error
# - am I root
# - are there background jobs?
prompt_status() {
  local symbols
  symbols=()
  [[ $RETVAL -ne 0 ]] && symbols+="%{%F{red}%}✘"
  #[[ $UID -eq 0 ]] && symbols+="%{%F{yellow}%}⚡"
  [[ $(jobs -l | wc -l) -gt 0 ]] && symbols+="%{%F{cyan}%}⚙"

  [[ -n "$symbols" ]] && prompt_segment white default "$symbols"
}

rhprompt_time() {
  echo -n "%{%F{$CRYPTO_HOSTCOLOR}%}"
  echo -n " $RSEGMENT_HOLLOW_SEPARATOR "
  echo -n "%D{%H:%M:%S}"
  echo -n "%{%f%k%}"
}

rhprompt_date() {
  echo -n "%{%F{$CRYPTO_HOSTCOLOR}%}"
  echo -n " $RSEGMENT_HOLLOW_SEPARATOR "
  echo -n "%D{%Y%m%d}"
  echo -n "%{%f%k%}"
}

rprompt_time() {
  local host_fg_color
  host_fg_color=`prompt_fgcolor "$CRYPTO_HOSTCOLOR"`
  rprompt_segment "$CRYPTO_HOSTCOLOR" "$host_fg_color" "%D{%H:%M:%S}"
}

rprompt_date() {
  rprompt_segment white black "%D{%Y%m%d}"
}

## Main prompt
build_prompt() {
  RETVAL=$?
  echo
  prompt_status
  prompt_virtualenv
  prompt_context
  prompt_dir
  if [[ "${plugins[(r)git]}" = "git" ]]; then
    prompt_git
  fi
  if [[ "${plugins[(r)hg]}" = "hg" ]]; then
    prompt_hg
  fi
  prompt_end
}

# looks funny because of spacing on the right of the terminal, but I would
# prefer this if I could...
build_rprompt() {
  CRYPTO_HOSTCOLOR=`prompt_color "$HOST"`
  rprompt_time
  #rprompt_date
}

build_rhprompt() {
  CRYPTO_HOSTCOLOR=`prompt_color "$HOST"`
  rhprompt_time
  #rhprompt_date
}

if [[ ! -z $ZSH_CRYPTOPOWER_LANG ]]; then
  export LC_ALL="$ZSH_CRYPTOPOWER_LANG"
fi
PROMPT='$(build_prompt) '
RPROMPT='$(build_rhprompt)'
